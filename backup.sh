function backup(){
    # mkdir ./backup--`date +%Y-%m-%d"_"%H_%M_%S` && cd "$_"
    mkdir ./backup-db-media/ && cd "$_"

    docker exec -t graphql_blog_db_1 pg_dumpall -c -U postgres > db.sql
    tar -zcvf db.tar.gz db.sql; rm -v db.sql

    cd ../server/staticfiles/ && tar -zcf media.tar.gz media/ && cd -;
    mv ../server/staticfiles/media.tar.gz media.tar.gz
}

function upload(){
    scp -r ./backup-db-media/ user@example.com:/www/project/
}

function load_data(){
    cd ./backup-db-media/
    tar -xvf db.tar.gz
    # cat your_dump.sql | docker exec -i graphql_blog_db_1 psql -U postgres

    tar -xf media.tar.gz --overwrite --directory ../server/staticfiles/
}

# backup
# upload
load_data
