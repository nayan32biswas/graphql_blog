import graphene
from django.contrib.auth import get_user_model
from .types import UserType
from graphql_jwt.decorators import login_required

User = get_user_model()


class Register(graphene.Mutation):
    user = graphene.Field(UserType)
    success = graphene.Boolean()

    class Arguments:
        email = graphene.String(required=True)
        password = graphene.String(required=True)
        confirm_password = graphene.String(required=True)

    @classmethod
    def mutate(self, root, info, **kwargs):
        if kwargs["password"] != kwargs["confirm_password"]:
            raise ValueError("password and confirm_password is not match")
        user = User.objects.create(email=kwargs["email"], password=kwargs["password"])
        user.save()
        return self(user=user, success=True)


class UserMutation(graphene.Mutation):
    user = graphene.Field(UserType)

    class Arguments:
        email = graphene.String()
        username = graphene.String()
        first_name = graphene.String()
        last_name = graphene.String()

    @classmethod
    @login_required
    def mutate(self, root, info, **kwargs):
        user = info.context.user
        update = False
        for key, value in kwargs.items():
            update = True
            user.__dict__[key] = value
        if update:
            user.save()
        return UserMutation(user=user)
