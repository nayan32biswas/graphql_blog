import graphene
import graphql_jwt

from django.contrib.auth import get_user_model
from graphql_jwt.decorators import login_required

from .types import UserType
from .mutations import Register, UserMutation

User = get_user_model()


class Query(graphene.ObjectType):
    # users = graphene.List(UserType)
    user = graphene.Field(UserType)

    # # Admin auth required
    # @login_required
    # def resolve_users(root, info):
    #     return User.objects.all()

    @login_required
    def resolve_user(root, info, **kwargs):
        return info.context.user


class Mutation(graphene.ObjectType):
    register = Register.Field()
    user = UserMutation.Field()

    token_auth = graphql_jwt.ObtainJSONWebToken.Field()
    verify_token = graphql_jwt.Verify.Field()
    refresh_token = graphql_jwt.Refresh.Field()
