import graphene
from graphene_django import DjangoObjectType
from django.contrib.auth import get_user_model

User = get_user_model()


class UserPublicType(DjangoObjectType):
    image_url = graphene.String()
    full_name = graphene.String()

    class Meta:
        model = User
        only_fields = (
            "username",
            "full_name",
            "profile_avatar",
            "role",
        )

    @staticmethod
    def resolve_image_url(root, info):
        return root.image_url

    @staticmethod
    def resolve_full_name(root, info):
        return root.full_name


class UserType(DjangoObjectType):
    class Meta:
        model = User
