# Api data

## Posts
```graphql
{
  posts(first:1) {
    pageInfo {
      startCursor
      endCursor
      hasNextPage
      hasPreviousPage
    }
    edges {
      cursor
      node {
        user {
          fullName
          imageUrl
        }
        title
        description
        comments(first:1){
          user{
            username
            fullName
          }
          description
          childs{
            description
          }
        }
      }
    }
  }
}
```

## Post

