from graphene.relay.node import from_global_id


def get_obj_by_id(model, id):
    _, id = from_global_id(global_id=id)
    return model.objects.get(id=id)


def is_owner(model, *args, **kwargs):
    # args minimum carry (cls, root, info)
    info = args[2]
    _, id = from_global_id(global_id=kwargs["id"])
    obj = model.objects.get(id=id)
    if obj.user != info.context.user:
        raise Exception("Object permission denied.")
    return obj


def permission(*deco_args, func=is_owner, model=None, **deco_kwargs):
    """
    parms will pass into decorator.
    execute_func will check actual permission like is_owner.
    """

    def decorator(original_func):
        """original_func is from where decorator seted"""

        def wrapper(*args, **kwargs):
            """args and kwargs as same as original function argument"""
            if model:
                kwargs["object"] = func(model, *args, **kwargs)
            return original_func(*args, **kwargs)

        return wrapper

    return decorator
