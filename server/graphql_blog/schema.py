import graphene


from account import schema as ac_schema
from post import schema as po_schema


class Query(ac_schema.Query, po_schema.Query, graphene.ObjectType):
    pass


class Mutation(ac_schema.Mutation, po_schema.Mutation, graphene.ObjectType):
    pass


schema = graphene.Schema(query=Query, mutation=Mutation)
