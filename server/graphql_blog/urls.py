from django.contrib import admin
from django.urls import path, include  # , re_path
from django.conf.urls.static import static
from django.contrib.auth import settings

# from django.views.generic import TemplateView
# from django.views.decorators.csrf import csrf_exempt

from graphene_django.views import GraphQLView
from graphql_jwt.decorators import jwt_cookie

from .schema import schema

urlpatterns = [
    path("admin/", admin.site.urls),
    path("graphql/", jwt_cookie(GraphQLView.as_view(graphiql=True, schema=schema))),
]

if settings.DEBUG:
    import debug_toolbar

    urlpatterns = (
        urlpatterns
        + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
        + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
    )

    urlpatterns += [
        path("__debug__/", include(debug_toolbar.urls)),
    ]

# urlpatterns += [
#     path("", TemplateView.as_view(template_name="index.html")),
#     re_path(r"^.*/$", TemplateView.as_view(template_name="index.html")),
# ]  # this line for reactjs
