from django.contrib import admin

from .models import Post, CommonImage, Comment

admin.site.register(Post)
admin.site.register(CommonImage)
admin.site.register(Comment)
