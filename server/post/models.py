from django.db import models
from django.contrib.auth import get_user_model
from taggit.managers import TaggableManager

User = get_user_model()


class PostManager(models.Manager):
    def published(self):
        return super().get_queryset().filter(is_published=True)


class Post(models.Model):
    user = models.ForeignKey(User, related_name="posts", on_delete=models.CASCADE)
    title = models.CharField(max_length=255)
    cover_image = models.ImageField(upload_to="post")
    alt = models.CharField(max_length=255, blank=True)
    description = models.TextField()

    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    is_published = models.BooleanField(default=True)

    tags = TaggableManager()
    objects = PostManager()

    class Meta:
        indexes = [models.Index(fields=["user", "title"])]

    def __str__(self):
        return self.title

    @property
    def image_url(self):
        return self.cover_image.url if self.cover_image else None


class CommonImage(models.Model):
    name = models.CharField(max_length=127)
    image = models.ImageField(upload_to="commonimage")
    alt = models.CharField(max_length=255, blank=True)
    created_at = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.name


class Comment(models.Model):
    user = models.ForeignKey(
        User, related_name="comments", on_delete=models.CASCADE, null=True
    )
    parent = models.ForeignKey(
        "self", related_name="childs", on_delete=models.CASCADE, blank=True, null=True
    )
    post = models.ForeignKey(
        "post.Post", related_name="comments", on_delete=models.CASCADE
    )
    description = models.TextField()
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        indexes = [models.Index(fields=["parent", "post"])]

    def __str__(self):
        return self.description[:20]

    def save(self, *args, **kwargs):
        if self.parent:
            # TODO: check parent has parent or not
            pass
        super().save(*args, **kwargs)
