import graphene
from django.contrib.auth import get_user_model
from graphql_jwt.decorators import login_required

from graphql_blog.utils import get_obj_by_id
from graphql_blog.permissions import permission, is_owner
from .models import Post, Comment
from .types import PostType, CommentType

User = get_user_model()


class PostCreate(graphene.Mutation):
    post = graphene.Field(PostType)

    class Arguments:
        title = graphene.String(required=True)
        cover_image = graphene.String()
        alt = graphene.String()
        description = graphene.String(required=True)
        is_published = graphene.String()
        tags = graphene.List(graphene.String)

    @classmethod
    @login_required
    def mutate(self, root, info, **kwargs):
        kwargs["user"] = info.context.user
        post = Post.objects.create(**kwargs)
        return self(post=post)


class PostUpdate(graphene.Mutation):
    post = graphene.Field(PostType)

    class Arguments:
        id = graphene.ID(required=True)
        title = graphene.String(required=True)
        cover_image = graphene.String()
        alt = graphene.String()
        description = graphene.String()
        is_published = graphene.String()
        tags = graphene.List(graphene.String)

    @classmethod
    @login_required
    @permission(func=is_owner, model=Post)
    def mutate(self, root, info, id, **kwargs):
        post = kwargs["object"]
        post.__dict__.update(**kwargs)
        post.save()
        return self(post=post)


class CommentCreate(graphene.Mutation):
    comment = graphene.Field(CommentType)

    class Arguments:
        parent = graphene.ID()
        post = graphene.ID(required=True)
        description = graphene.String(required=True)

    @classmethod
    @login_required
    def mutate(self, root, info, post, **kwargs):
        kwargs["user"] = info.context.user
        kwargs["post"] = get_obj_by_id(Post, post)
        kwargs["parent"] = (
            get_obj_by_id(Comment, kwargs["parent"]) if "parent" in kwargs else None
        )
        comment = Comment.objects.create(**kwargs)
        comment.save()
        return self(comment=comment)


class CommentUpdate(graphene.Mutation):
    comment = graphene.Field(CommentType)

    class Arguments:
        id = graphene.ID(required=True)
        description = graphene.String(required=True)

    @classmethod
    @login_required
    @permission(func=is_owner, model=Comment)
    def mutate(self, root, info, id, **kwargs):
        comment = kwargs["object"]
        comment.__dict__.update(**kwargs)
        comment.save()
        return self(comment=comment)
