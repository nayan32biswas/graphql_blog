from django.contrib.auth import get_user_model

import graphene
from graphene_django.filter import DjangoFilterConnectionField

from .models import Post
from .types import PostType
from .mutations import PostCreate, PostUpdate, CommentCreate, CommentUpdate

User = get_user_model()


class Query(graphene.ObjectType):

    posts = DjangoFilterConnectionField(PostType)

    post = graphene.Field(
        PostType,
        id=graphene.Argument(
            graphene.ID,
            description="ID of the post.",
        ),
        description="Look up a post by ID.",
    )

    def resolve_posts(root, info, *args, **kwargs):
        return Post.objects.published()

    def resolve_post(root, info, *args, **kwargs):
        return Post.objects.published().first()


class Mutation(graphene.ObjectType):
    create_post = PostCreate.Field()
    update_post = PostUpdate.Field()
    create_comment = CommentCreate.Field()
    update_comment = CommentUpdate.Field()
