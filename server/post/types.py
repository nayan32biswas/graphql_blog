import graphene
from graphene_django import DjangoObjectType
from .models import Post, Comment
from account.types import UserPublicType


class PostType(DjangoObjectType):
    user = graphene.Field(UserPublicType)
    image_url = graphene.String()
    comments = graphene.List(lambda: CommentType, first=graphene.Int())

    class Meta:
        model = Post
        interfaces = (graphene.relay.Node,)
        filter_fields = {"title": ["exact", "icontains", "istartswith"]}
        only_fields = (
            "user",
            "title",
            "cover_image",
            "alt",
            "description",
            "is_published",
        )

    @staticmethod
    def resolve_user(root, info):
        return root.user

    @staticmethod
    def resolve_image_url(root, info):
        return root.image_url

    @staticmethod
    def resolve_comments(root, info, *args, first=None, **kwargs):
        comments = Comment.objects.filter(post=root, parent__isnull=True)
        return comments[:first] if first else comments


class CommentType(DjangoObjectType):
    user = graphene.Field(UserPublicType)
    childs = graphene.List(lambda: SubCommentType, first=graphene.Int())

    class Meta:
        model = Comment
        interfaces = (graphene.relay.Node,)
        only_fields = ("childs", "id", "description")

    @staticmethod
    def resolve_user(root, info):
        return root.user

    @staticmethod
    def resolve_childs(root, info, *args, first=None, **kwargs):
        childs = root.childs.all()
        return childs[:first] if first else childs


class SubCommentType(DjangoObjectType):
    user = graphene.Field(UserPublicType)

    class Meta:
        model = Comment
        interfaces = (graphene.relay.Node,)
        only_fields = ("description",)

    @staticmethod
    def resolve_user(root, info):
        return root.user
